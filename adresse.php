<?php

class Adresse{
    private $id;
    private $rue;
    private $num;
    private $localite;
    private $cp;
    private $pays;

    function __construct($rue, $num, $localite, $cp, $pays){
        //$this -> setId($id);
        $this -> setRue($rue);
        $this -> setNum($num);
        $this -> setLocalite($localite);
        $this -> setCp($cp);
        $this -> setPays($pays);

}

// Setters
public function setId($data){
    $this -> id = $data;
}

public function setRue($data){
    $this -> rue = $data;
}

public function setNum($data){
    $this -> num = $data;
}

public function setLocalite($data){
    $this -> localite = $data;
}

public function setCp($data){
    $this -> cp = $data;
}

public function setPays($data){
    $this -> pays= $data;
}

// Getters

public function getId(){
    return $this -> id;
}
public function getRue(){
    return $this -> rue;
}

public function getNum(){
    return $this -> num;
}

public function getLocalite(){
    return $this -> localite;
}

public function getCp(){
    return $this -> cp;
}

public function getPays(){
    return $this -> pays;
}

}

?>